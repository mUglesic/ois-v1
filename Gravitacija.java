import java.util.Scanner;
import java.lang.Math;


class Gravitacija {
    //public static final double C=0.00000000006674;
    //public static final double r=0.000006371;
    //public static final double M=0.000000000000000000000005972;
    public static final double C = 6.674 * Math.pow(10, -11);
    public static final double r = 6.371 * Math.pow(10, 6);
    public static final double M = 5.972 * Math.pow(10, 24);
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("OIS je zakon!");
        double visina = sc.nextInt();
        double gravitacijski_pospesek = (C*M/((r+visina)*(r+visina)));
        printVisinaInPospesek(visina, gravitacijski_pospesek);
        
    }
    
    static void printVisinaInPospesek(double visina, double pospesek) {
        System.out.println(visina + " " + pospesek);
    }
    
}